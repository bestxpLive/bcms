<?php

include __DIR__ . '/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use  \Tracy\Debugger;

if (getenv('mode') == 'dev') {
    Debugger::enable();
}

$request = Request::createFromGlobals();

$app = new \BCMS\App\Core();


$app->handle($request)->send();;
